package utilidades_practica;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {// hacemos la conexion con la base de datos
    public String databasename = "hospital";
    public String ip = "localhost";
    public String port = "3306";
    public String user = "root";
    public String pass= "Samanta3007";

    //hacemos el contructor con el user, pass
    public Conexion(String databasename, String user, String pass) {
        this.databasename = databasename;
        this.user = user;
        this.pass = pass;
    }
    //todos los datos del contructor,

    public Conexion(String databasename, String ip, String port, String user, String pass) {
        this.databasename = databasename;
        this.ip = ip;
        this.port = port;
        this.user = user;
        this.pass = pass;
    }
    //contructor vacio
    public Conexion() {
    }

    //hacemos el metodo conexion
    public Connection getConnection(){
        String url = "jdbc:mysql://" + this.ip + ":" + this.port + "/" + this.databasename; // la url de conexion
        Connection con = null;
        try { // establecer la conexion y lanzar una exepcion
            con = DriverManager.getConnection(url,this.user , this.pass);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return con;
    }
}
