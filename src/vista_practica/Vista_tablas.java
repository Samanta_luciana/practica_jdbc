package vista_practica;

import controlador_practica.MedicamentosControllerJDBC;
import controlador_practica.PacientesControllerJDBC;
import controlador_practica.RecetasControllerJDBC;
import modelo_practica.Medicamentos;
import modelo_practica.Pacientes;
import modelo_practica.Recetas;
import utilidades_practica.Conexion;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Vista_tablas {
    public static void main(String[] args) {
        Conexion conexion =new Conexion();
        Connection con = conexion.getConnection();
        PacientesControllerJDBC controller = new PacientesControllerJDBC(con);
        MedicamentosControllerJDBC medicamentosController = new MedicamentosControllerJDBC(con);
        RecetasControllerJDBC recetasController = new RecetasControllerJDBC(con);


              Scanner scanner = new Scanner(System.in);
              boolean salir =false;
        while (!salir) {
            // Mostrar menú para seleccionar tabla
            System.out.println("Seleccione la tabla sobre la que desea operar:");
            System.out.println("1. Pacientes");
            System.out.println("2. Medicamentos");
            System.out.println("3. Recetas");
            System.out.println("4. Salir");
            System.out.print("SELECCIONE UNA TABLA: ");
            int tablaSeleccionada = scanner.nextInt();
            scanner.nextLine(); // Limpiar el buffer de entrada

            switch (tablaSeleccionada) {
                case 1:
                    // Operaciones sobre la tabla Pacientes
                    menuPacientes(con,controller); // espera la instancia de conexion y el metodo
                    break;
                case 2:
                    menuMedicamentos(con,medicamentosController); // espera la instancia de conexion y el metodo
                    break;
                case 3:
                    menuRecetas(con,recetasController);
                    break;
                case 4:
                    salir = true;
                    break;
                default:
                    System.out.println("Opción inválida. Por favor, seleccione una opción válida.");
            }
        }

        scanner.close();
    }

    // Menú de operaciones sobre la tabla Pacientes
    public static void menuPacientes(Connection con, PacientesControllerJDBC pacientesController){
        Scanner scanner = new Scanner(System.in);
        boolean salir = false;
        while (!salir) {
            // Mostrar menú de operaciones para la tabla Pacientes
            System.out.println();
            System.out.println("Menú Tabla: Pacientes:");
            System.out.println("1. Mostrar todos los Pacientes");
            System.out.println("2. Buscar por ID");
            System.out.println("3. Buscar por Nombre");
            System.out.println("4. El promedio de Edad de los Pacientes");
            System.out.println("5. Insertar Paciente ");
            System.out.println("6. Borra paciente por ID");
            System.out.println("7. Actualizar paciente por ID");
            System.out.println("8. Volver al menú principal");
            System.out.print("Seleccione una opción: ");
            int opcion = scanner.nextInt();
            scanner.nextLine(); // Limpiar el buffer de entrada

            switch (opcion) {
                case 1:
                    System.out.println("  ");
                    // Mostrar todos los pacientes
                    ArrayList<Pacientes> lista = pacientesController.mostrarTodos();
                    for (Pacientes p : lista) {
                        System.out.println(p);
                    }
                    break;
                case 2:
                    //BUSCAR por id , LO pedimos al usuario
                    System.out.println("ingrese el id del Paciente");
                    int idPacienteBuscar = scanner.nextInt(); // se lee el num. y se alamcena en la variable idPacienteBuscar
                    scanner.nextLine();

                    //llamamos al metodo
                    Pacientes pacientesEncontradosID = pacientesController.buscarPorID(idPacienteBuscar);
                    if (pacientesEncontradosID != null){
                        System.out.println( " Encontras el paciente ");
                        System.out.println(pacientesEncontradosID);
                    }else{
                        System.out.println("No hay ningun paciente con ese ID");
                    }
                    break;
                case 3:
                    System.out.println(" Ingrese el nombre del paciente");
                    String nombrePaciente = scanner.nextLine();
                    // Mostrar todos los pacientes
                    ArrayList<Pacientes>  registro_nombre= pacientesController.buscarPorNombre(nombrePaciente);
                    for (Pacientes p : registro_nombre  ) {
                        System.out.println(p);
                    }
                    break;
                case 4:
                    double promedioEdad = pacientesController.obtenerPromedioEdad();
                    System.out.println();
                    System.out.println( "El promedio de edad de los pacientes es: " + promedioEdad);
                    System.out.println();

                    break;
                case 5:
                    System.out.println("Ingrese el nombre del paciente:");
                    String nombre = scanner.nextLine();
                    System.out.println("Ingrese el apellido del paciente:");
                    String apellido = scanner.nextLine();
                    System.out.println("Ingrese la dirección del paciente:");
                    String direccion = scanner.nextLine();
                    System.out.println("Ingrese la fecha de nacimiento del paciente (AAAA-MM-DD):");
                    String fechaNacimientoStr = scanner.nextLine();

                    // Crear un objeto Pacientes con los datos ingresados pasarle los datos y hacer set en paciente
                    Pacientes nuevoPaciente = new Pacientes(1,"Nombre","Apellido","Direccion",new Date());
                    nuevoPaciente.setNombre(nombre);
                    nuevoPaciente.setApellido(apellido);
                    nuevoPaciente.setDireccion(direccion);
                    nuevoPaciente.setFecha_nacimiento(convertirAFecha(fechaNacimientoStr));

                    // Insertar el nuevo paciente utilizando el controlador
                    boolean insercionExitosa = pacientesController.insertarPaciente(nuevoPaciente);
                    if (insercionExitosa) {
                        System.out.println("El paciente se ha insertado correctamente en la base de datos.");
                    } else {
                        System.out.println("Hubo un error al insertar el paciente en la base de datos.");
                    }  // habia cerrado el scanner y me daba error, no hacerlo

                    break;


                case 6:
                    // Pedir ID a borrar
                    System.out.println("Ingrese el ID del paciente que desea borrar:");
                    int idPacienteABorrar = scanner.nextInt();
                    scanner.nextLine();

                    // Llamar al método para borrar el paciente por su ID
                    boolean eliminacionExitosa = pacientesController.borrarPaciente(idPacienteABorrar);

                    // Verificamos si se elimino
                    if (eliminacionExitosa) {
                        System.out.println("El paciente se ha eliminado correctamente de la base de datos.");
                    } else {
                        System.out.println("No se encontró ningún paciente con el ID proporcionado en la base de datos.");
                    }
                    break;


                case 7:
                    // Pedir id para actualizar
                    System.out.println("Ingrese el ID del paciente que desea actualizar:");
                    int idPacienteActualizar = scanner.nextInt();
                    scanner.nextLine(); // Limpiar el buffer de entrada

                    // Obtener los nuevos datos del paciente
                    System.out.println("Ingrese el nuevo nombre del paciente:");
                    String nuevoNombre = scanner.nextLine();
                    System.out.println("Ingrese el nuevo apellido del paciente:");
                    String nuevoApellido = scanner.nextLine();
                    System.out.println("Ingrese la nueva dirección del paciente:");
                    String nuevaDireccion = scanner.nextLine();
                    System.out.println("Ingrese la nueva fecha de nacimiento del paciente (AAAA-MM-DD):");
                    String nuevaFechaNacimientoStr = scanner.nextLine();

                    // Crear un objeto Pacientes con los nuevos datos
                    Pacientes pacienteActualizado = new Pacientes(idPacienteActualizar, nuevoNombre, nuevoApellido, nuevaDireccion, convertirAFecha(nuevaFechaNacimientoStr));

                    // Llamar al método actualizarPaciente del controlador
                    boolean actualizacionExitosa = pacientesController.actualizarPaciente(pacienteActualizado);
                    if (actualizacionExitosa) {
                        System.out.println("El paciente se ha actualizado correctamente en la base de datos.");
                    } else {
                        System.out.println("Hubo un error al actualizar el paciente en la base de datos.");
                    }
                    break;

                case 8:
                    salir = true;
                    break;

                default:
                    System.out.println("Opción inválida. Por favor, seleccione una opción válida.");


            }
        }
    }

    // menu operaciones sobre tabla medicamentos

    public static void menuMedicamentos(Connection con, MedicamentosControllerJDBC medicamentosControllerJDBC) {
        Scanner scanner = new Scanner(System.in);
        boolean salir = false;
        while (!salir) {
            // Mostrar menú de operaciones para la tabla Pacientes
            System.out.println();
            System.out.println("Menú Tabla: Medicamentos:");
            System.out.println("1. Mostrar todos los medicamentos");
            System.out.println("2. Buscar por ID");
            System.out.println("3. El precio total de los Medicamentos ");
            System.out.println("4. Insertar Medicamento");
            System.out.println("5. Borrar Medicamento por ID");
            System.out.println("6. Volver al menú principal");
            System.out.print("Seleccione una opción: ");
            int opcion = scanner.nextInt();
            scanner.nextLine(); // Limpiar el buffer de entrada

            switch (opcion) {
                case 1:
                    System.out.println("  ");
                    // Mostrar todos los medicamentos
                    ArrayList<Medicamentos> lista_medicamentos = medicamentosControllerJDBC.mostrarTodosMedicamentos();
                    for (Medicamentos m : lista_medicamentos) {
                        System.out.println(m);
                    }
                    break;
                case 2:
                   //BUSCAR por id , LO pedimos al usuario
                    System.out.println("Ingrese el ID del Medicamento");
                    int idMedicamentoBuscar = scanner.nextInt(); // se lee el num. y se alamcena en la variable id
                    scanner.nextLine();

                    //llamamos al metodo
                    Medicamentos medicamentosEncontradosID = medicamentosControllerJDBC.buscarPorIDMedicamento(idMedicamentoBuscar);
                    if (medicamentosEncontradosID != null){
                        System.out.println("Encontraste el Medicamento");
                        System.out.println(medicamentosEncontradosID);
                    }else{
                        System.out.println("No hay ningun Medicamento con ese ID");
                    }
                    break;

                case 3:
                    double precioTotalMedicamentos = medicamentosControllerJDBC.obtenerTotalPrecioMedicamentos();
                    System.out.println();
                    System.out.println( "El TOTAL de precios de los Medicamentos  en nuestra base de datos es: " + precioTotalMedicamentos);
                    System.out.println();

                    break;
                 case 4:
                    System.out.println("Ingrese el nombre del Medicamento:");
                    String nombre = scanner.nextLine();
                    System.out.println("Ingrese el precio del Medicamento:");
                    double precio = Double.parseDouble(scanner.nextLine());
                    System.out.println("Ingrese el origen del Medicamento:");
                    String origen = scanner.nextLine();
                    System.out.println("Ingrese la fecha de caducidad del medicamento (AAAA-MM-DD):");
                    String fechaMedicamentoStr = scanner.nextLine();

                    // Crear un objeto Pacientes con los datos ingresados pasarle los datos y hacer set en paciente
                    Medicamentos nuevoMedicamento = new Medicamentos(1,"Nombre",precio,"origen",new Date());
                    nuevoMedicamento.setNombre(nombre);
                    nuevoMedicamento.setPrecio(precio);
                    nuevoMedicamento.setOrigen(origen);
                    nuevoMedicamento.setFecha_caducidad(convertirAFecha(fechaMedicamentoStr));

                    // Insertar el nuevo paciente utilizando el controlador
                    boolean insercionExitosa = medicamentosControllerJDBC .insertarMedicamentos(nuevoMedicamento);
                    if (insercionExitosa) {
                        System.out.println("El Medicamento se ha insertado correctamente en la base de datos.");
                    } else {
                        System.out.println("Hubo un error al insertar el Medicamento en la base de datos.");
                    }  // habia cerrado el scanner y me daba error, no hacerlo

                    break;


                 case 5:
                    // Pedir ID a borrar
                    System.out.println("Ingrese el ID del Medicamento que desea borrar:");
                    int id_medicamentoBorrar = scanner.nextInt();
                    scanner.nextLine();

                    // Llamar al método para borrar el paciente por su ID
                    boolean eliminacionExitosa = medicamentosControllerJDBC.borrarMedicamento(id_medicamentoBorrar);

                    // Verificamos si se elimino
                    if (eliminacionExitosa) {
                        System.out.println("El Medicamento se ha eliminado correctamente de la base de datos.");
                    } else {
                        System.out.println("No se encontró ningún Medicamento con el ID proporcionado en la base de datos.");
                    }
                    break;


                case 6:
                    salir = true;
                    break;

                default:
                    System.out.println("Opción inválida. Por favor, seleccione una opción válida.");

            }
        }
    }

    // menu operaciones sobre tabla Recetas
    public static void menuRecetas(Connection con, RecetasControllerJDBC recetasControllerJDBC) {
        Scanner scanner = new Scanner(System.in);
        boolean salir = false;
        while (!salir) {
            // Mostrar menú de operaciones para la tabla Pacientes
            System.out.println();
            System.out.println("Menú Tabla: Recetas:");
            System.out.println("1. Mostrar todas las Recetas");
            System.out.println("2. Buscar por ID");
            System.out.println("3. El total de las Recetas ");
            System.out.println("4. Insertar Recetas");
            System.out.println("5. Actualizar Receta por ID");
            System.out.println("6. Volver al menú principal");
            System.out.print("Seleccione una opción: ");
            int opcion = scanner.nextInt();
            scanner.nextLine(); // Limpiar el buffer de entrada

            switch (opcion) {
                case 1:
                    System.out.println("  ");
                    // Mostrar todas las recetas
                    ArrayList<Recetas> lista_recetas = recetasControllerJDBC.mostrarRecetas();
                    for (Recetas r : lista_recetas) {
                        System.out.println(r);
                    }
                    break;
                case 2:
                    //BUSCAR por id , LO pedimos al usuario
                    System.out.println("Ingrese el ID de la Receta");
                    int idRecetasBuscar = scanner.nextInt(); // se lee el num. y se alamcena en la variable id
                    scanner.nextLine();

                    //llamamos al metodo (los datos del metodo)
                    Recetas recetasEncontradasID = recetasControllerJDBC.buscarPorIDrecetas(idRecetasBuscar); //llamar al metodo q esta en controller y dentro la variable que creamos
                    if (recetasEncontradasID != null) { //este lo creamos en controller es nuestro objeto recetas
                        System.out.println("Encontraste la Receta");
                        System.out.println(recetasEncontradasID);
                    } else {
                        System.out.println("No hay ninguna Receta con ese ID");
                    }
                    break;


                case 3:
                    double total= recetasControllerJDBC.obtenerTotalRecetas();
                    System.out.println();
                    System.out.println( "El TOTAL de las Recetas en nuestra base de datos es: " + total);
                    System.out.println();

                    break;
                case 4:
                    System.out.println("Ingrese ID de la Receta:");
                    int idReceta = scanner.nextInt();

                    System.out.println("Ingrese ID del Paciente:");
                    int idPaciente = scanner.nextInt();

                    System.out.println("Ingrese ID del Medicamento:");
                    int idMedicamento = scanner.nextInt();

                    System.out.println("Ingrese la fecha de la Receta nueva para el Paciente (AAAA-MM-DD):");
                    String fechaRecetaStr = scanner.nextLine(); // Consume la nueva línea en blanco
                    fechaRecetaStr = scanner.nextLine(); // Lee la fecha de la receta

                    System.out.println("Ingrese cuántas Recetas le dio al Paciente:");
                    int cantidad = Integer.parseInt(scanner.nextLine());

                    // Crear un objeto Recetas con los datos ingresados
                    Recetas nuevaReceta = new Recetas(idReceta, idPaciente, idMedicamento, convertirAFecha(fechaRecetaStr), cantidad);

                    // Insertar la nueva receta utilizando el controlador
                    boolean insercionExitosa = recetasControllerJDBC.insertarRecetas(nuevaReceta);
                    if (insercionExitosa) {
                        System.out.println("La Receta se ha insertado correctamente en la base de datos.");
                    } else {
                        System.out.println("Hubo un error al insertar la Receta en la base de datos.");
                    }
                    break;


                /*case 5:
                    // Pedir ID a borrar
                    System.out.println("Ingrese el ID del Medicamento que desea borrar:");
                    int id_medicamentoBorrar = scanner.nextInt();
                    scanner.nextLine();

                    // Llamar al método para borrar el paciente por su ID
                    boolean eliminacionExitosa = medicamentosControllerJDBC.borrarMedicamento(id_medicamentoBorrar);

                    // Verificamos si se elimino
                    if (eliminacionExitosa) {
                        System.out.println("El Medicamento se ha eliminado correctamente de la base de datos.");
                    } else {
                        System.out.println("No se encontró ningún Medicamento con el ID proporcionado en la base de datos.");
                    }
                    break;*/


                case 6:
                    salir = true;
                    break;

                default:
                    System.out.println("Opción inválida. Por favor, seleccione una opción válida.");


                    scanner.close();

            }
        }
    }


            // Método para convertir una cadena de fecha en un objeto java.util.Date
            private static Date convertirAFecha (String fechaStr){
                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    return formato.parse(fechaStr);
                } catch (ParseException e) {
                    System.out.println("Error al convertir la fecha.");
                    return null;
                }
            }

        }





























