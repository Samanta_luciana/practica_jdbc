package modelo_practica;

import java.util.Date;

    public class Medicamentos{
        public int id_medicamento;
        public String nombre;
        public double precio;
        public String origen;
        public Date fecha_caducidad;


        //hacemos el contructor
        public Medicamentos(int id_medicamento, String nombre, double precio, String origen, Date fecha_caducidad) {
            this.id_medicamento = id_medicamento;
            this.nombre = nombre;
            this.precio = precio;
            this.origen = origen;
            this.fecha_caducidad = fecha_caducidad;

        }
        //hacer los get y set por que los necesito


        public int getId_medicamento() {
            return id_medicamento;
        }

        public void setId_medicamento(int id_medicamento) {
            this.id_medicamento = id_medicamento;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public double getPrecio() {
            return precio;
        }

        public void setPrecio(double precio) {
            this.precio = precio;
        }

        public String getOrigen() {
            return origen;
        }

        public void setOrigen(String origen) {
            this.origen = origen;
        }

        public Date getFecha_caducidad() {
            return fecha_caducidad;
        }

        public void setFecha_caducidad(Date fecha_caducidad) {
            this.fecha_caducidad = fecha_caducidad;
        }

        @Override
        public String toString() { // metodo
            return "Medicamentos: " +
                    "ID_Medicamento: " + id_medicamento +
                    ", Nombre: " + nombre +
                    ", Precio: " + precio +
                    ", Origen: " + origen  +
                    ", Fecha_Caducidad:" + fecha_caducidad ;
        }
    }

