package modelo_practica;

import java.util.Date;

public class Recetas {
    public int id_receta;
    public int id_paciente;
    public int id_medicamento;
    public Date fecha_receta;
    public int cantidad;

    //contructor

    public Recetas(int id_receta, int id_paciente, int id_medicamento, Date fecha_receta, int cantidad) {
        this.id_receta = id_receta;
        this.id_paciente = id_paciente;
        this.id_medicamento = id_medicamento;
        this.fecha_receta = fecha_receta;
        this.cantidad = cantidad;
    }

    public int getId_receta() {
        return id_receta;
    }

    public void setId_receta(int id_receta) {
        this.id_receta = id_receta;
    }

    public int getId_paciente() {
        return id_paciente;
    }

    public void setId_paciente(int id_paciente) {
        this.id_paciente = id_paciente;
    }

    public int getId_medicamento() {
        return id_medicamento;
    }

    public void setId_medicamento(int id_medicamento) {
        this.id_medicamento = id_medicamento;
    }

    public Date getFecha_receta() {
        return fecha_receta;
    }

    public void setFecha_receta(Date fecha_receta) {
        this.fecha_receta = fecha_receta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "Recetas{" +
                "id_receta=" + id_receta +
                ", id_paciente=" + id_paciente +
                ", id_medicamento=" + id_medicamento +
                ", fecha_receta=" + fecha_receta +
                ", cantidad=" + cantidad +
                '}';
    }
}
