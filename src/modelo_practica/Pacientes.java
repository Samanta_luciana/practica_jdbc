package modelo_practica;

import java.util.Date;

public class Pacientes {
    public int id_paciente;
    public String nombre;
    public String apellido;
    public String direccion;
    public Date fecha_nacimiento;

    public Pacientes(int id_paciente, String nombre, String apellido, String direccion, Date fecha_nacimiento) {
        this.id_paciente = id_paciente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.fecha_nacimiento = fecha_nacimiento;
    }

    @Override
    public String toString() {
        return "PACIENTE: " +
                "ID_Paciente: " + id_paciente +
                ", Nombre: " + nombre + ' ' +
                ", Apellido: " + apellido + ' ' +
                ", Direccion:" + direccion + ' ' +
                ", Fecha_Nacimiento: " + fecha_nacimiento;
    }



    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setId_paciente(int id_paciente) {
        this.id_paciente = id_paciente;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public int getId_paciente() {
        return id_paciente;
    }
}
