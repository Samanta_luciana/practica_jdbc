package controlador_practica;

import modelo_practica.Pacientes;

import java.util.ArrayList;

public interface PacientesDAO {
    ArrayList<Pacientes> mostrarTodos();
    Pacientes buscarPorID(int id);
    ArrayList<Pacientes> buscarPorNombre(String nombre);
    double obtenerPromedioEdad();
    boolean insertarPaciente(Pacientes paciente);
    boolean borrarPaciente(int id);
    boolean actualizarPaciente (Pacientes paciente);


}
