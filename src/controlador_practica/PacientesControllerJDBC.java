package controlador_practica;
import modelo_practica.Pacientes;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

public class PacientesControllerJDBC implements PacientesDAO {
    Connection con;

    public final String mostrarTodos = "SELECT * FROM pacientes"; //mostrar todos los datos
    public final String buscarPorID = "SELECT * FROM  pacientes where id_paciente = ?";
    public final String buscarPorNOMBRE = "SELECT * FROM pacientes where nombre = ?";
    public final String obtenerPromedioEdad = "SELECT AVG(YEAR(CURDATE()) - YEAR(fecha_nacimiento)) AS Promedio_edad FROM pacientes";
    public final String insertarPaciente = "INSERT pacientes (nombre, apellido, direccion,fecha_nacimiento) values (?,?,?,?)";
    public final String borrarPaciente = "DELETE FROM pacientes where id_paciente = ?";
    public final String actualizarPaciente = "UPDATE pacientes SET nombre = ?, apellido = ?, direccion = ?, fecha_nacimiento = ? WHERE id_paciente = ?";

    public PacientesControllerJDBC(Connection con) {  // establecer la conexion para comunicarse con la bbdd
        this.con = con;
    }


    @Override
    public ArrayList<Pacientes> mostrarTodos() { //VER TODOS LOS PACIENTES
        ArrayList<Pacientes> lista = new ArrayList<Pacientes>();  //creamos una lista de pacientes
        try {
            PreparedStatement statement = con.prepareStatement(mostrarTodos); //Preparar una sentencia que se ejecuta en mysql
            ResultSet rs = statement.executeQuery();

            //interactuamos sobre cada fila
            while (rs.next()) {
                int id_paciente = rs.getInt("ID_paciente");
                String nombre = rs.getString("Nombre");
                String apellido = rs.getString("Apellido");
                String direccion = rs.getString("Direccion");
                Date fecha_nacimiento = rs.getDate("Fecha_Nacimiento");

                Pacientes p1 = new Pacientes(id_paciente, nombre, apellido, direccion, fecha_nacimiento); //creamos al paciente1
                // lista de pacientes
                lista.add(p1);
            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return lista;
    }

    @Override
    public Pacientes buscarPorID(int id) { //BUSCAR POR ID
        Pacientes pacientesEncontradosId = null; //la variable se inicia en null para indicar que no se encontros un paciente con ese id
        try {
            PreparedStatement statement = con.prepareStatement(buscarPorID);//preparar la cosulta
            statement.setInt(1, id);// parametro de la consulta
            ResultSet rs = statement.executeQuery();//esjecutar la consulta

            if (rs.next()) {//verificar si encontro un paciente con el ID que especificamos
                //obtener los datos del pacientede la fila actual del ResulSet
                int id_paciente = rs.getInt("ID_paciente");
                String nombre = rs.getString("Nombre");
                String apellido = rs.getString("Apellido");
                String direccion = rs.getString("Direccion");
                Date fecha_nacimiento = rs.getDate("Fecha_Nacimiento");

                //creamos un objeto Paciente con los datos.
                pacientesEncontradosId = new Pacientes(id_paciente, nombre, apellido, direccion, fecha_nacimiento);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return pacientesEncontradosId;
    }

    @Override
    public ArrayList<Pacientes> buscarPorNombre(String nombre) { //BUSCAR POR NOMBRE
        ArrayList<Pacientes> registo_nombre = new ArrayList<>(); //se crea una lista que contendra los pacientes
        try {
            PreparedStatement statement = con.prepareStatement(buscarPorNOMBRE); // preparar consulta sql
            statement.setString(1, nombre);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int id_paciente = rs.getInt("ID_Paciente");
                String apellido = rs.getString("Apellido");
                String direccion = rs.getString("Direccion");
                Date fecha_nacimiento = rs.getDate("Fecha_Nacimiento");

                Pacientes paciente = new Pacientes(id_paciente, nombre, apellido, direccion, fecha_nacimiento);
                registo_nombre.add(paciente);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return registo_nombre;
    }

    @Override
    public double obtenerPromedioEdad() { //OBTENER PROMEDIO DE EDAD
        double promedioEdad = 0;
        try { //preparamos la consulta
            PreparedStatement statement = con.prepareStatement(obtenerPromedioEdad);
            ResultSet rs = statement.executeQuery(); // ejecutar consulta

            //obtener resultado de la consulta
            if (rs.next()) {
                promedioEdad = rs.getDouble("Promedio_Edad");

            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return promedioEdad;
    }

    @Override
    public boolean insertarPaciente(Pacientes paciente) { // INSERTAR PACIENTE TENIENDO EN CUENTA EL ID EN MI BBDD
        try {
            PreparedStatement statement = con.prepareStatement(insertarPaciente, Statement.RETURN_GENERATED_KEYS);
            //lo que hace RETURN..es una const que inserta datos en una tabla con la columna de la clave pri.autoincremental
            statement.setString(1, paciente.getNombre()); //los parametos que tengo en mi tabla
            statement.setString(2, paciente.getApellido());
            statement.setString(3, paciente.getDireccion());
            java.sql.Date fechaSQL = new java.sql.Date(paciente.getFecha_nacimiento().getTime());
            statement.setDate(4, fechaSQL);

            int filasInsertadas = statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                int idGenerado = generatedKeys.getInt(1);
                paciente.setId_paciente(idGenerado); // Actualizamos el ID generado en el objeto Paciente
            }
            return filasInsertadas > 0;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public boolean borrarPaciente(int id) {
        try {
            // Preparar la sentencia SQL para eliminar el paciente con el ID proporcionado
            PreparedStatement statement = con.prepareStatement(borrarPaciente);
            statement.setInt(1, id);

            // Ejecutar la sentencia SQL, almacenamos valor  y verificamos condicion
            int filasAfectadas = statement.executeUpdate();

            // Verificar si se eliminó exitosamente al menos una fila
            return filasAfectadas > 0;
        } catch (SQLException e) {
            // Manejar cualquier excepción de SQL
            throw new RuntimeException("Error al intentar eliminar el paciente con ID " + id, e);
        }
    }


    @Override
    public boolean actualizarPaciente(Pacientes paciente) {
        try { //preparamos consulta
            PreparedStatement statement = con.prepareStatement(actualizarPaciente);
            statement.setString(1, paciente.getNombre());
            statement.setString(2, paciente.getApellido());
            statement.setString(3, paciente.getDireccion());
            statement.setDate(4, new java.sql.Date(paciente.getFecha_nacimiento().getTime()));
            statement.setInt(5, paciente.getId_paciente());


            // Ejecutar la sentencia SQL de actualización y obtener el número de filas afectadas
            int filasAfectadas = statement.executeUpdate();

            // Verificar si se actualizó al menos una fila
            return filasAfectadas > 0;


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }


}
