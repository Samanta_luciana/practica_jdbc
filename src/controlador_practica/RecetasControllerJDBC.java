package controlador_practica;

import modelo_practica.Recetas;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

public class RecetasControllerJDBC implements  RecetasDAO {
    Connection con;

    public final String mostrarRecetas = "SELECT * FROM recetas"; //mostrar todos los datos
    public final String buscarPorIDrecetas = "SELECT * FROM  recetas where id_receta = ?";
    public final String obtenerTotalRecetas = "SELECT SUM(cantidad) AS cantidad FROM recetas";
    public final String insertarRecetas = "INSERT recetas(id_paciente, id_medicamento,fecha_receta,cantidad) VALUES (?,?,?,?)";
    public final String actualizarRecetas = "UPDATE recetas SET id_paciente = ?, id_medicamento = ?, fecha_receta = ?, cantidad = ? WHERE id_receta = ?";

    public RecetasControllerJDBC(Connection con) {  //constructor
        this.con = con;
    }

    @Override
    public ArrayList<Recetas> mostrarRecetas() { //VER TODOS Las recetas
        ArrayList<Recetas> lista_recetas = new ArrayList<Recetas>();  //creamos una lista de pacientes
        try {
            PreparedStatement statement = con.prepareStatement(mostrarRecetas); //Preparar una sentencia que se ejecuta en mysql
            ResultSet rs = statement.executeQuery();

            //interactuamos sobre cada fila
            while (rs.next()) {
                int id_receta = rs.getInt("id_receta");
                int id_paciente = rs.getInt("id_paciente");
                int id_medicamento = rs.getInt("id_medicamento");
                Date fecha_receta = rs.getDate("Fecha_receta");
                int cantidad = rs.getInt("cantidad");

                Recetas r1 = new Recetas(id_receta, id_paciente, id_medicamento, fecha_receta, cantidad); //creamos la receta1
                // lista de recetas
                lista_recetas.add(r1);
            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return lista_recetas;
    }

    @Override
    public Recetas buscarPorIDrecetas(int id) {
        Recetas recetasEncontradasID = null; //iniciamos la variable en null para decir que no se enontro ninguna receta
        try {
            PreparedStatement statement = con.prepareStatement(buscarPorIDrecetas);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery(); //ejecutar consulta
            if (rs.next()) {
                //obtener los datos del medicamento
                int id_receta = rs.getInt("id_receta");
                int id_paciente = rs.getInt("id_paciente");
                int id_medicamento = rs.getInt("id_medicamento");
                Date fecha_receta = rs.getDate("fecha_receta");
                int cantidad = rs.getInt("cantidad");

                //hacemos un medicamento esto luego va en la vista
                recetasEncontradasID = new Recetas(id_receta, id_paciente, id_medicamento, fecha_receta, cantidad);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return recetasEncontradasID;
    }

    @Override
    public double obtenerTotalRecetas() {
        double totalRecetas = 0;
        try {
            PreparedStatement statement = con.prepareStatement(obtenerTotalRecetas);
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                totalRecetas = rs.getDouble("cantidad");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return totalRecetas;
    }


    @Override
    public boolean insertarRecetas(Recetas recetas) {
        // INSERTAR  TENIENDO EN CUENTA EL ID EN MI BBDD
        try {
            PreparedStatement statement = con.prepareStatement(insertarRecetas, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, recetas.getId_paciente());
            statement.setInt(2, recetas.getId_medicamento());
            java.sql.Date fechaSQL = new java.sql.Date(recetas.getFecha_receta().getTime());
            statement.setDate(3, fechaSQL);
            statement.setInt(4, recetas.getCantidad());

            int filasInsertadas = statement.executeUpdate();

            // Obtener el ID generado automáticamente
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                int idGenerado = generatedKeys.getInt(1);
                System.out.println("Se ha insertado la receta con ID " + idGenerado);
            }

            return filasInsertadas > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }




        @Override
        public boolean actualizarRecetas (Recetas receta){
            return false;
        }
    }

