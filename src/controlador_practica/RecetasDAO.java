package controlador_practica;

import modelo_practica.Pacientes;
import modelo_practica.Recetas;

import java.util.ArrayList;
import java.util.Date;

public interface RecetasDAO {
    ArrayList<Recetas> mostrarRecetas();
    Recetas buscarPorIDrecetas(int id);
    double obtenerTotalRecetas();
    boolean insertarRecetas(Recetas receta);

    boolean actualizarRecetas (Recetas receta);

}

