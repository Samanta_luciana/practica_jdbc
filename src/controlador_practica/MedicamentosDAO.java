package controlador_practica;

import modelo_practica.Medicamentos;
import modelo_practica.Pacientes;

import java.util.ArrayList;

public interface MedicamentosDAO {
    ArrayList<Medicamentos> mostrarTodosMedicamentos();
    Medicamentos buscarPorIDMedicamento(int id);
    double obtenerTotalPrecioMedicamentos();
    boolean insertarMedicamentos(Medicamentos medicamento);
    boolean borrarMedicamento(int id);
    boolean actualizarMedicamento (Medicamentos medicamento);


}

