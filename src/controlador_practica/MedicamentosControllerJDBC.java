package controlador_practica;

import modelo_practica.Medicamentos;
import modelo_practica.Pacientes;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

public class MedicamentosControllerJDBC implements MedicamentosDAO {
    Connection con;

    public final String mostrarTodosMedicamentos = "SELECT * FROM medicamentos"; //mostrar todos los datos
    public final String buscarPorIDMedicamentos = "SELECT * FROM  medicamentos where id_medicamento = ?";
    public final String obtenerTotalPrecioMedicamentos = "SELECT SUM(precio) AS total FROM medicamentos;";
    public final String insertarMedicamentos = "INSERT medicamentos(nombre, precio, origen,fecha_caducidad) values (?,?,?,?)";
    public final String borrarMedicamentos= "DELETE FROM medicamentos where id_medicamento = ?";
    public final String actualizarActualizarMedicamento= "UPDATE medicamentos SET nombre = ?, precio = ?, origen = ?, fecha_caducidad = ? WHERE id_medicamento = ?";

 //generar contructor
    public MedicamentosControllerJDBC(Connection con) {
        this.con = con;
    }

    @Override
    public ArrayList<Medicamentos> mostrarTodosMedicamentos() {
        ArrayList<Medicamentos> lista_medicamento = new ArrayList<Medicamentos>();

        try {
            PreparedStatement statement = con.prepareStatement(mostrarTodosMedicamentos);
            ResultSet rs = statement.executeQuery();

            //interactuamos en la tabla,
            while(rs.next()) {
                int id_medicamento = rs.getInt("id_medicamento");
                String nombre = rs.getString("nombre");
                double precio = rs.getDouble("precio");
                String origen = rs.getString("origen");
                Date fecha_caducidad = rs.getDate("fecha_caducidad");

                //creas el objeto
                Medicamentos m1 = new Medicamentos(id_medicamento, nombre, precio, origen, fecha_caducidad);

                lista_medicamento.add(m1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return lista_medicamento;
    }

    @Override
    public Medicamentos buscarPorIDMedicamento(int id) {
        Medicamentos medicamentosEncontradosID = null; //iniciamos la variable en null para decir que no se enontro ningun medicamento
        try{
            PreparedStatement statement = con.prepareStatement(buscarPorIDMedicamentos);
            statement.setInt (1,id);
            ResultSet rs = statement.executeQuery(); //ejecutar consulta
            if(rs.next()){
                //obtener los datos del medicamento
                 int id_medicamento = rs.getInt("id_medicamento");
                String nombre = rs.getString("nombre");
                double precio = rs.getDouble("precio");
                String origen = rs.getString("origen");
                Date fecha_caducidad =rs.getDate("fecha_caducidad");

                //hacemos un medicamento
                medicamentosEncontradosID = new Medicamentos(id_medicamento,nombre,precio,origen,fecha_caducidad);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return medicamentosEncontradosID;
    }

    @Override
    public double obtenerTotalPrecioMedicamentos() {
        double precioTotalMedicamentos = 0;
        try{
            PreparedStatement statement = con.prepareStatement(obtenerTotalPrecioMedicamentos);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                precioTotalMedicamentos = rs.getDouble("total");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return precioTotalMedicamentos;
    }

    @Override
    public boolean insertarMedicamentos(Medicamentos medicamento) {
    // INSERTAR medicamento TENIENDO EN CUENTA EL ID EN MI BBDD
            try {
                PreparedStatement statement = con.prepareStatement(insertarMedicamentos, Statement.RETURN_GENERATED_KEYS);
                //lo que hace RETURN..es una const que inserta datos en una tabla con la columna de la clave pri.autoincremental
                statement.setString(1, medicamento.getNombre()); //los parametos que tengo en mi tabla
                statement.setDouble(2, medicamento.getPrecio());
                statement.setString(3, medicamento.getOrigen());
                java.sql.Date fechaSQL = new java.sql.Date(medicamento.getFecha_caducidad().getTime());
                statement.setDate(4, fechaSQL);

                int filasInsertadas = statement.executeUpdate();
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    int idGenerado = generatedKeys.getInt(1);
                    medicamento.setId_medicamento(idGenerado); // Actualizamos el ID generado en el objeto Paciente
                }
                return filasInsertadas > 0;

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }



    @Override
    public boolean borrarMedicamento(int id) {
        try {
            // Preparar la sentencia SQL para eliminar el medicamento con el ID proporcionado
            PreparedStatement statement = con.prepareStatement(borrarMedicamentos);
            statement.setInt(1, id);

            // Ejecutar la sentencia SQL, almacenamos valor  y verificamos condicion
            int filasAfectadas = statement.executeUpdate();

            // Verificar si se eliminó exitosamente al menos una fila
            return filasAfectadas > 0;
        } catch (SQLException e) {
            // Manejar cualquier excepción de SQL
            throw new RuntimeException("Error al intentar eliminar el Medicamento con ID " + id, e);
        }
    }


    @Override
    public boolean actualizarMedicamento(Medicamentos medicamento) {
        return false;
    }
}
