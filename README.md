# Práctica JDBC

Este proyecto consiste en un programa Java que utiliza JDBC (Java Database Connectivity) y sigue la arquitectura MVC (Modelo-Vista-Controlador) para interactuar con una base de datos relacional a través de consultas SQL. El programa ofrece un menú interactivo por terminal que permite ejecutar diversas consultas sobre las tablas de la base de datos.

## Requisitos del Proyecto

- Crear o buscar una base de datos que contenga al menos 3 tablas, siendo al menos 2 de ellas con más de 5 atributos.
- Implementar un proyecto Java que utilice JDBC y siga la arquitectura MVC.
- El menú interactivo por terminal debe ofrecer las siguientes opciones de consulta sobre las tablas:
  1. Mostrar todos los datos de una tabla a elección.
  2. Mostrar todos los datos de una tabla según una condición (por ejemplo, buscar por ID o nombre).
  3. Mostrar datos utilizando una función de agrupación (como MAX, MIN, AVG, SUM) de una tabla a elección.
  4. Insertar datos en una tabla a elección.
  5. Borrar datos por clave primaria de una tabla a elección.
  6. Actualizar todos los parámetros de una tabla a elección.
  7. Actualizar un parámetro específico de una tabla a elección.
- El menú debe solicitar al usuario los datos necesarios para realizar la consulta y la tabla sobre la que se ejecutará, mostrando luego el resultado. Si el resultado es false o null, debe mostrar el mensaje correspondiente.
- Gestionar todas las excepciones de forma adecuada para garantizar la estabilidad y la seguridad del programa.

